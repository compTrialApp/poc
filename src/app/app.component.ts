import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'trialApp';
  opened: boolean;

  CloseSidebar() {
    this.opened = false
  }

  ToggleSideNav() {
    this.opened = !this.opened
  }
}
