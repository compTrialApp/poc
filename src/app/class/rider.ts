import {ScoreEntry} from "./scoreEntry";

export class Rider {
  private start: Date = new Date();
  private end: Date = new Date();
  public scoreEntries: ScoreEntry[] = [];

  constructor(public name: string = "",
              public number: number = null) {
  }

  IsValid() {
    return this.name.length > 0 && this.number != null;
  }

  IsEmpty() {
    return this.name.length <= 0 && (this.number == null || this.number <= 0);
  }

  static FromObject(d: Object): Rider {
    return Object.assign(new Rider(), d);
  }

  AddScoreEntry(zoneId: number, timer: number, counters: number[]) {
    let roundsCount = this.GetRoundFromZone(zoneId);
    let exists = this.SearchForScore(zoneId, roundsCount);
    if (exists == null) {
      this.scoreEntries.push(new ScoreEntry(timer, roundsCount, zoneId, counters));
      return;
    }
    exists.timer = timer;
    exists.counters = counters;
  }

  GetRoundFromZone(zoneId: number): number {
    let roundsCount = 0;
    this.scoreEntries.forEach(score => {
      if (score.zone == zoneId)
        roundsCount++;
    });
    return roundsCount;
  }

  SearchForScore(zoneId: number, roundId: number): ScoreEntry {
    let ret: ScoreEntry = null;
    this.scoreEntries.forEach(score => {
      if (score.zone == zoneId && score.round == roundId)
        ret = score;
    });
    return ret;
  }
}
