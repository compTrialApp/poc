import { Rider } from './rider';
import {Competition} from "./competition";

describe('Rider', () => {
  it('should create an instance', () => {
    expect(new Rider(new Competition(), "example", 42)).toBeTruthy();
  });
});
