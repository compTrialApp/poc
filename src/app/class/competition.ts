import {Counter} from "./counter";
import * as duration from 'duration-fns';

export class Competition {
  public totalTime: number = 3 * 60 * 60;
  public zoneTime: number = 2 * 60;
  public zoneNb: number = 5;
  public roundNb: number = 3;
  public counters: Counter[] = [
    new Counter("feet", 0, 5, 1),
    new Counter("points", 0, 60, 10)
  ];

  constructor(formValue: {
    roundNb: number,
    compTime: string,
    zoneNb: number,
    zoneTime: string,
    useFeet: boolean,
    usePoints: boolean
  } = {
    roundNb: 3,
    compTime: "3h",
    zoneNb: 5,
    zoneTime: "2m",
    useFeet: true,
    usePoints: false
  }) {
    this.totalTime = duration.toSeconds("PT" + formValue.compTime.toUpperCase());
    this.zoneTime = duration.toSeconds("PT" + formValue.zoneTime.toUpperCase());
    this.zoneNb = formValue.zoneNb;
    this.roundNb = formValue.roundNb;
    this.counters[0].active = formValue.useFeet;
    this.counters[1].active = formValue.usePoints;
  }

  ToJSON() {
    return JSON.stringify(this);
  }

  static FromObject(d: Object): Competition {
    if (d == null)
      return null;
    let obj = Object.assign(new Competition(), d);
    let counters: Counter[] = [];
    obj.counters.forEach((c) => counters.push(Counter.Clone(c)));
    obj.counters = counters;
    return obj;
  }
}
