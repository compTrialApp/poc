export class Counter {
  public name: string
  public active: boolean = true
  public start: number
  public end: number
  public step: number

  constructor(name: string, start: number, end: number, step: number) {
    this.name = name
    this.start = start
    this.end = end
    this.step = step
  }

  static Clone(d: Counter): Counter {
    return new Counter(d.name, d.start, d.end, d.step)
  }
}
