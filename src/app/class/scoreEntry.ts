export class ScoreEntry {
  constructor(public timer: number = 0,
              public round: number = 0,
              public zone: number = 0,
              public counters: number[] = []) {
  }
}
