import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TopBarComponent} from './sharedComponents/top-bar/top-bar.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {BotBarComponent} from './sharedComponents/bot-bar/bot-bar.component';
import {MatButtonModule} from "@angular/material/button";
import {MatDividerModule} from "@angular/material/divider";
import { RunComponent } from './components/run/run.component';
import { RiderSelectComponent } from './sharedComponents/run/rider-select/rider-select.component';
import { TimerComponent } from './sharedComponents/run/timer/timer.component';
import { ScoreEntryComponent } from './sharedComponents/run/score-entry/score-entry.component';
import { SummaryComponent } from './sharedComponents/run/summary/summary.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ResultComponent } from './components/result/result.component';
import { CompSetupComponent } from './components/comp-setup/comp-setup.component';
import { RidersSetupComponent } from './components/riders-setup/riders-setup.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {CdkTableModule} from "@angular/cdk/table";
import {MatIconModule} from "@angular/material/icon";
import { SidebarComponent } from './sharedComponents/sidebar/sidebar.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import {CountdownModule} from "ngx-countdown";
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    BotBarComponent,
    RunComponent,
    RiderSelectComponent,
    TimerComponent,
    ScoreEntryComponent,
    SummaryComponent,
    CompSetupComponent,
    ResultComponent,
    CompSetupComponent,
    RidersSetupComponent,
    SidebarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    CdkTableModule,
    MatIconModule,
    MatSidenavModule,
    CountdownModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
