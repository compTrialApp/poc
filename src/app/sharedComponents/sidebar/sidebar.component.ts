import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Output() closeSidebar = new EventEmitter();

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  GoToCompSetup() {
    this.router.navigate(['/comp'])
    this.closeSidebar.emit()
  }

  GoToRidersSetup() {
    this.router.navigate(['/riders'])
    this.closeSidebar.emit()
  }

  GoToZone() {
    this.router.navigate(['/run'])
    this.closeSidebar.emit()
  }

  GoToResult() {
    this.router.navigate(['/result'])
    this.closeSidebar.emit()
  }
}
