import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RiderSelectComponent } from './rider-select.component';

describe('RiderSelectComponent', () => {
  let component: RiderSelectComponent;
  let fixture: ComponentFixture<RiderSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RiderSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RiderSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
