import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AppManager} from 'src/app/services/app-manager.service';
import {SelectState} from "../../../enums/select-state";
import {Rider} from "../../../class/rider";

@Component({
  selector: 'app-rider-select',
  templateUrl: './rider-select.component.html',
  styleUrls: ['./rider-select.component.scss']
})
export class RiderSelectComponent implements OnInit {
  @Output() riderSelectDone = new EventEmitter();
  allSelectState = SelectState;
  state: SelectState = SelectState.Rider;

  constructor(public appManager: AppManager) {
  }

  ngOnInit(): void {
  }

  GetRiders(): Rider[] {
    return this.appManager.GetRiders();
  }

  SelectRider(id: number) {
    this.appManager.SetCurrentRider(id);
    this.state = SelectState.Zone;
  }

  SelectZone(id: number) {
    this.appManager.SetCurrentZone(id);
    this.riderSelectDone.emit();
  }

  GetZoneNb(): number[] {
    return Array(this.appManager.GetCompetition().zoneNb);
  }

  AllRoundDone(zoneId: number): boolean {
    let roundsCount = this.appManager.GetCurrentRider().GetRoundFromZone(zoneId);
    return roundsCount >= this.appManager.GetCompetition().roundNb;
  }

}
