import {ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {TimerState} from 'src/app/enums/timer-state';
import {AppManager} from 'src/app/services/app-manager.service';
import {CountdownComponent, CountdownConfig} from "ngx-countdown";

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimerComponent implements OnInit {
  @Output() timerDone = new EventEmitter();
  @ViewChild('cd') cd: CountdownComponent;
  allTimerState = TimerState;
  state: TimerState = TimerState.Start;
  config: CountdownConfig;

  constructor(public appManager: AppManager) {
  }

  ngOnInit(): void {
    this.config = {
      demand: true,
      leftTime: this.appManager.GetCompetition().zoneTime,
      format: 'mm:ss'
    };
  }

  StartTimer() {
    this.cd.begin();
    this.state = TimerState.Counting;
  }

  PauseTimer() {
    this.cd.pause();
    this.state = TimerState.Paused;
  }

  ResumeTimer() {
    this.cd.resume();
    this.state = TimerState.Counting;
  }

  StopTimer() {
    this.cd.stop();
    this.timerDone.emit(this.cd.left / 1000);
  }
}
