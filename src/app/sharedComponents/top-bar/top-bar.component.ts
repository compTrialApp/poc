import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],

})
export class TopBarComponent implements OnInit {
  @Output() changeSidebar = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  ToggleSideNav() {
    this.changeSidebar.emit();
  }

}
