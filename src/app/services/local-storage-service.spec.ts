import { TestBed } from '@angular/core/testing';

import { AppManager } from './app-manager.service';

describe('LocalStorageService', () => {
  let service: AppManager;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppManager);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
