import {Injectable} from '@angular/core';
import {Competition} from "../class/competition";
import {Rider} from "../class/rider";

@Injectable({
  providedIn: 'root'
})
export class AppManager {
  private comp: Competition;
  private riders: Rider[];
  private curId: number;
  private curZone: number;

  constructor() {
  }

  GetCompetition = (): Competition => this.comp;

  SetCompetition(comp: Competition) {
    this.comp = comp;
  }

  GetRiders = (): Rider[] => this.riders;

  SetCurrentRider(id: number) {
    if (id >= 0 && id < this.riders.length)
      this.curId = id;
  }

  GetCurrentRider = (): Rider => this.riders[this.curId];

  SetCurrentZone(id: number) {
    this.curZone = id;
  }

  GetCurrentZoneId = () => this.curZone;

  SetRiders(riders: Rider[]) {
    this.riders = riders;
  }

  SaveToLocalStorage() {
    localStorage.setItem("comp", this.comp.ToJSON());
    localStorage.setItem("riders", JSON.stringify(this.riders));
  }

  LoadFromLocalStorage() {
    let json = localStorage.getItem("comp");
    let compJson = JSON.parse(json);
    this.comp = Competition.FromObject(compJson);

    json = localStorage.getItem("riders");
    let ridersJson = JSON.parse(json);
    this.riders = [];
    if (ridersJson != null && ridersJson.length > 0)
      ridersJson.forEach((r: any) => {
          let rider = Rider.FromObject(r);
          this.riders.push(rider);
        }
      );
  }
}
