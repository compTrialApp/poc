export enum RunState {
  RiderSelect,
  Timer,
  ScoreEntry,
  Summary
}
