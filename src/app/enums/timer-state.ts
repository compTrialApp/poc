export enum TimerState {
  Start = 0,
  Counting = 1,
  Paused = 2
}
