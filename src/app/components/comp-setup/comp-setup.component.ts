import {Component, EventEmitter, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Competition} from 'src/app/class/competition';
import {AppManager} from "../../services/app-manager.service";
import * as duration from 'duration-fns';
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";


@Component({
  selector: 'app-comp-setup',
  templateUrl: './comp-setup.component.html',
  styleUrls: ['./comp-setup.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CompSetupComponent implements OnInit {
  @Output() compSetupDone = new EventEmitter();

  private positiveNumberRegex = /^[1-9]+[0-9]*$/;
  private durationRegex = /(((,|h|and|&)+)?([\d.]+)\s?(hours|hour|h|minutes|min|m|seconds|sec|s|days|day|d|$))+/;

  public competitionForm: FormGroup;
  private formBuilder: FormBuilder;

  constructor(formBuilder: FormBuilder, private appManager: AppManager, private router: Router) {
    this.formBuilder = formBuilder;
    this.competitionForm = null;
  }

  ngOnInit(): void {
    this.appManager.LoadFromLocalStorage();
    let comp = this.appManager.GetCompetition();
    if (comp == null)
      comp = new Competition();
    let compTime = `${duration.toUnit({seconds: comp.totalTime}, "hours")}h`;
    let zoneTime = `${duration.toUnit({seconds: comp.zoneTime}, "minutes")}m`;
    this.competitionForm = this.formBuilder.group({
      roundNb: [comp.roundNb, [Validators.required, Validators.pattern(this.positiveNumberRegex)]],
      compTime: [compTime, [Validators.required, Validators.pattern(this.durationRegex)]],
      zoneNb: [comp.zoneNb, [Validators.required, Validators.pattern(this.positiveNumberRegex)]],
      zoneTime: [zoneTime, [Validators.required, Validators.pattern(this.durationRegex)]],
      useFeet: [comp.counters[0].active, [Validators.required]],
      usePoints: [comp.counters[1].active, [Validators.required]]
    });
  }

  OnSubmit() {
    if (!this.competitionForm.valid)
      return;
    this.appManager.SetCompetition(new Competition(this.competitionForm.value));
    this.appManager.SaveToLocalStorage();
    this.router.navigate(["/run"]);
  }
}
