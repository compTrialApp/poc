import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompSetupComponent } from './comp-setup.component';

describe('CompetSetupComponent', () => {
  let component: CompSetupComponent;
  let fixture: ComponentFixture<CompSetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompSetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
