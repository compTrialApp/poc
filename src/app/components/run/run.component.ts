import {Component, OnInit} from '@angular/core';
import {RunState} from "../../enums/run-state";
import {AppManager} from "../../services/app-manager.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-run',
  templateUrl: './run.component.html',
  styleUrls: ['./run.component.scss']
})
export class RunComponent implements OnInit {
  allRunState = RunState;
  state: RunState = RunState.RiderSelect;

  constructor(private appManager: AppManager, private router: Router) {
  }

  ngOnInit(): void {
    this.appManager.LoadFromLocalStorage();
    let comp = this.appManager.GetCompetition();
    if (comp == null) {
      this.router.navigate(["/comp"]);
      return;
    }
    let riders = this.appManager.GetRiders();
    if (riders == null || riders.length <= 0)
      this.router.navigate(["/riders"]);
  }

  RiderSelectDone() {
    this.state = RunState.Timer;
  }

  TimerDone(timer: number) {
    this.appManager.GetCurrentRider().AddScoreEntry(
      this.appManager.GetCurrentZoneId(),
      timer,
      []
    );
    this.appManager.SaveToLocalStorage();
    this.state = RunState.RiderSelect;
  }
}
