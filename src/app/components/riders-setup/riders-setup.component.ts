import {Component, EventEmitter, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {Rider} from "../../class/rider";
import {Competition} from "../../class/competition";
import {AppManager} from "../../services/app-manager.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";

@Component({
  selector: 'app-riders-setup',
  templateUrl: './riders-setup.component.html',
  styleUrls: ['./riders-setup.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RidersSetupComponent implements OnInit {
  @Output() riderSetupDone = new EventEmitter();
  riders: Rider[] = [];
  comp: Competition;

  constructor(private appManager: AppManager, private _snackBar: MatSnackBar, private router: Router) {
  }

  ngOnInit(): void {
    this.appManager.LoadFromLocalStorage();
    this.comp = this.appManager.GetCompetition();
    this.riders = this.appManager.GetRiders();
    if (this.riders == null)
      this.riders = [];
    if (this.riders.length <= 0)
      this.riders.push(new Rider());
    if (this.comp == undefined)
      this.comp = new Competition();
  }

  DeleteRiders(id: number) {
    if (this.riders.length <= 1)
      return;
    this.riders.splice(id, 1);
  }

  AddRider() {
    this.riders.push((new Rider()));
  }

  SubmitRiders() {
    let valid = true;
    let toRemove = [];
    for (let i = 0; i < this.riders.length; i++) {
      const value = this.riders[i];
      if (value.IsEmpty()) {
        toRemove.push(i);
        continue;
      }
      if (!value.IsValid()) {
        valid = false;
        break;
      }
    }
    toRemove.forEach(idx => {
      this.riders.splice(idx, 1);
    });
    if (!valid) {
      this._snackBar.open("invalid rider");
      return;
    }
    this.appManager.SetRiders(this.riders);
    this.appManager.SaveToLocalStorage();
    this.router.navigate(["/run"]);
  }
}
