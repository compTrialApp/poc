import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RidersSetupComponent } from './riders-setup.component';

describe('RidersSetupComponent', () => {
  let component: RidersSetupComponent;
  let fixture: ComponentFixture<RidersSetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RidersSetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RidersSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
