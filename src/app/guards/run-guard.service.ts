import {Injectable} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  // Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import {Observable} from 'rxjs';
// import {AppManager} from "../services/app-manager.service";

@Injectable({
  providedIn: 'root'
})
export class RunGuard implements CanActivate {
  constructor(
    // private localStorage: AppManager, private router: Router
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // if (this.localStorage.GetCompetition() == undefined || this.localStorage.GetRiders() == undefined)
    //   this.router.navigate(["/"])
    return true;
  }
}
