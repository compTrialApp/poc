import { TestBed } from '@angular/core/testing';

import { RunGuard } from './run-guard.service';

describe('RunGuard', () => {
  let guard: RunGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(RunGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
