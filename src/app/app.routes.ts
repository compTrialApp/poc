import {Routes} from '@angular/router';
import {ResultComponent} from './components/result/result.component';
import {RunComponent} from "./components/run/run.component";
import {RunGuard} from "./guards/run-guard.service";
import {CompSetupComponent} from "./components/comp-setup/comp-setup.component";
import {RidersSetupComponent} from "./components/riders-setup/riders-setup.component";

export const routes: Routes = [
  {path: '', redirectTo: '/run', pathMatch: 'full'},
  {path: 'comp', component: CompSetupComponent},
  {path: 'riders', component: RidersSetupComponent},
  {path: 'run', component: RunComponent, canActivate: [RunGuard]},
  {path: 'result', component: ResultComponent},
];
